# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

### 因為開學時申請帳號時
### 106062209的帳號沒有收到驗證信
### 導致帳號無法使用
### 所以使用的是以下的網址
### Project URL: https://swiftfo142857.gitlab.io/AS_01_WebCanvas/

Basic control tools (30%)
- [V] Brush and eraser
- [V] Color selector
- [V] Simple menu (brush size)
- [V] Text input (10%)
    User can type texts on canvas
    Font menu (typeface and size)
- [V] Cursor icon (10%)
    The image should change according to the currently used tool
- [V] Refresh button (10%)
        Reset canvas 
- [V] Different brush shapes (15%)
    Circle, rectangle and triangle (5% for each shape)
- [V] Un/Re-do button (10%)
- [V] Image tool (5%)
    User can upload image and paste it 
- [V] Download (5%)
    Download current canvas as an image file

## something special

### mode : show the status of user
status      |meaning
------------|------------
idle        |waiting for input
draw        |drawing
text        |texting
circle      |drawing circle
triangle    |drawing triangle
rectangle   |drawing rectangle

### Cursor will change if you choose differnt size of pen and eraser

### function introduction

#### show the mode
![](mode.png)
#### control the size of brush and color
![](font.png)
#### reset the canvas
![](clear.png)
#### choose the pen or eraser
![](pen_eraser.png)
#### draw different shapes
![](shapes.png)
#### text button
![](btn-text.png)
#### submenu inside the text mode
![](text.png)
## how to add text to canvas
1. Click the text button
2. Click the place you want to text
3. Select the font size and font style
4. Click V to submit your change
5. If you want to leave text mode, click X

#### undo redo
![](undoredo.png)
#### upload save
![](upload_save.png)
