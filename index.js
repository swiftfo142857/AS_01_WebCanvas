console.log("Hello")
var canvas = document.getElementById("myCanvas")
var ctx = canvas.getContext("2d");
var slider = document.getElementById("myRange");
var modeMonitor = document.getElementById("modeMonitor")
var lw = document.getElementById("lineWidth")
var cursor = document.getElementById("cursor")
var color = document.getElementById('color')
var textInputGroup = document.getElementById('text-input-group')
var textInput = document.getElementById('text')
var btnFontDrop = document.getElementById('btnFontDrop')
var btnFontSizeDrop = document.getElementById('btnFontSizeDrop')

var lastX = 0
var lastY = 0
var modes = ['idle','draw','text','circle','triangle','rectangle']
var mode = 'idle'
var type_of_pen = "pen"
var history_image = []
var width = 600
var height = 400
var font_name = "Arial"
var font_size = "10"
var id = 0

slider.value = 10
lw.innerHTML = 10

setMode('idle')
history_image.push(document.getElementById('myCanvas').toDataURL());

function setMode(edit_mode){
    mode = edit_mode
    modeMonitor.innerHTML = "mode: " + mode
}
// slider controls size of pen
slider.addEventListener('change',function() {
    console.log("width change")
    lw.innerHTML = " "+ slider.value; // Display the default slider value
    ctx.lineWidth = slider.value;
    cursor.style.width = ctx.lineWidth;
    cursor.style.height = ctx.lineWidth;
})
//


color.addEventListener("change",function(){
    console.log("color select")
    if(type_of_pen == "eraser"){
        ctx.strokeStyle = "white"
    }
    else{
        ctx.strokeStyle = color.value;
    }
    
})
canvas.addEventListener("mousedown", draw)
canvas.addEventListener("mouseup", stopDraw)
canvas.addEventListener("mousemove",move)
canvas.addEventListener("mouseout",function() {
    cursor.style.display="none"
})


function draw(event){
    //if(mode != 'draw')  return
    lastX = event.offsetX
    lastY = event.offsetY
    ctx.strokeStyle = color.value;
    if(mode=="text"){
        textInputGroup.style.display = "block"
        textInputGroup.style.left = lastX
        textInputGroup.style.top = lastY
        return
    }
    else if(mode=="circle"){
        
        ctx.beginPath();
        ctx.arc(lastX, lastY, 40, 0, 2 * Math.PI);
        ctx.stroke();
        return
    }
    else if(mode=="rectangle"){
        ctx.strokeRect(lastX, lastY, 150, 100);
        return
    }
    else if(mode=="triangle"){
        var tri_height = 100
        var tri_width = 100
        ctx.moveTo(lastX, lastY); // pick up "pen," reposition at 500 (horiz), 0 (vert)
        ctx.lineTo(lastX-tri_width/2, lastY+tri_height); // draw straight down by 200px (200 + 200)
        ctx.lineTo(lastX+tri_width/2, lastY+tri_height); // draw up toward left (100 less than 300, so left)
        ctx.closePath(); // connect end to start
        ctx.stroke(); // out
        return
    }
    //console.log("drawing")
    
    setMode('draw')
    if(type_of_pen == "pen"){

    }
    else if(type_of_pen == "eraser"){
        ctx.strokeStyle="white"
    }
    ctx.beginPath();
    ctx.lineCap = "round";
}

function stopDraw(event){
    console.log("stop drawing")
    id = id + 1
    if (id < history_image.length) { history_image.length = id; }
    history_image.push(document.getElementById('myCanvas').toDataURL());

    if(mode == 'draw'){
        setMode('idle')
    }
}

function move(event){
    console.log(`${event.offsetX} ${event.offsetY}`)
    console.log(`${event.clientX} ${event.clientY}`)
    if((mode == 'draw' || mode == 'idle') && event.offsetX < width && event.offsetY < height){
        document.getElementById("cursor").style.display = "block";
        document.getElementById("cursor").style.left=event.offsetX + 14
        document.getElementById("cursor").style.top=event.offsetY
        document.getElementById("cursor").style.background=color.value;
        document.getElementById("cursor").style.border = "none";
        
        //console.log(type_of_pen)
        if(type_of_pen=="eraser"){
            document.getElementById("cursor").style.border = "2px solid gray";
            document.getElementById("cursor").style.background = "white";
        }
    }
    else {
        document.getElementById("cursor").style.display = "none";
    }
    if(mode!='draw') return
    //console.log("moving")
    var x = event.offsetX
    var y = event.offsetY
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(x, y);
    ctx.stroke();
    //console.log(`${lastX} ${lastY}`)
    //console.log(`${x} ${y}`)
    lastX = x
    lastY = y
}

function clearCanvas(){
    canvas.height = canvas.height
}

function text(){
    ctx.globalCompositeOperation = 'source-over';
    setMode('text')
    canvas.style.cursor = "text"
}

function submitText(){
    ctx.font = font_size+"px "+font_name;
    console.log(ctx.font)
    ctx.fillText(textInput.value, lastX, lastY);
    textInput.value = ""
}
function cancelText(){
    textInput.value = ""
    textInputGroup.style.display = "none"
    canvas.style.cursor = "none"
    setMode('idle')
}
function selectFont(font_name) {
    console.log(font_name)
    this.font_name = font_name
    btnFontDrop.innerHTML = font_name
}
function selectFontSize(font_size) {
    btnFontSizeDrop.innerHTML = font_size
    this.font_size = font_size
}
function erase(){
    console.log("eraser")
    ctx.globalCompositeOperation = 'destination-out';
    ctx.strokeStyle = "white"
    type_of_pen = "eraser"
}
function pen(){
    setMode('idle')
    ctx.globalCompositeOperation = 'source-over';
    ctx.strokeStyle = color.value
    type_of_pen = "pen"
}
function circle(){
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "pointer"
    setMode('circle')
}
function triangle(){
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "pointer"
    setMode('triangle')
}
function rectangle(){
    ctx.globalCompositeOperation = 'source-over';
    canvas.style.cursor = "pointer"
    setMode('rectangle')
}

function goTo(num) {
    console.log(history_image)
    id = id + num
    var Pic = new Image();
    Pic.src = history_image[id];
    Pic.onload = function () { 
    ctx.clearRect(0, 0, width, height);
    ctx.drawImage(Pic, 0, 0); 
    }
}
var picture = document.getElementById('upload');
picture.addEventListener('change', (e) => {
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            //canvas.width = img.width;
            //canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]); 
});

/*



*/

/*

function drawShape(type){
    console.log(type)
    
}
function move(event){
    
    console.log("moving")
    var x = event.offsetX
    var y = event.offsetY
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(x, y);
    ctx.stroke();
    //console.log(`${lastX} ${lastY}`)
    //console.log(`${x} ${y}`)
    lastX = x
    lastY = y
}
function getPos(event){
    var x = event.offsetX
    var y = event.offsetY
    return {
        'x':x,
        'y':y
    }
}
function notDraw(){
    console.log("not draw")
    canvas.removeEventListener("mousemove",move)
}
function text(){

}
function undo(){
    
}
function redo(){

}
function clearCanvas(){
    canvas.height = canvas.height
}
function pen(){
    
    ctx.strokeStyle = "FF0000"
    document.getElementById("cursor").style.backgroundColor = "black"
    canvas.addEventListener("mouseenter",showCursor)
}
function erase(){
    console.log("eraser")
    ctx.strokeStyle = "white"
}
function circle(){
    
}
function showCursor(){
    canvas.addEventListener("mousemove",()=>{
        document.getElementById("cursor").style.left=event.offsetX-ctx.lineWidth/2;
        document.getElementById("cursor").style.top=event.offsetY-ctx.lineWidth/2;
    })
}

canvas.addEventListener("mousedown",draw)
canvas.addEventListener("mouseup",function() {
    var imgData = ctx.getImageData(0, 0, 600, 400);
    history_image.push(imgData)
    console.log(history_image)
    canvas.removeEventListener("mousemove",move)
})
canvas.addEventListener("mouseenter",showCursor)
canvas.removeEventListener("mouseout",showCursor)
var color = document.getElementById('color')
color.addEventListener("change",function(){
    console.log("color select")
    ctx.strokeStyle = color.value;
})
*/